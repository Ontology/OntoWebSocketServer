﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.WebSocketServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;
using WebSocketSharp.Server;

namespace OntoWebSocketServer
{
    class Program
    {
        private static clsLocalConfig localConfig;
        private static ServiceAgent_Elastic serviceAgentDb;
        private static List<WebSocketService> serviceItems;
        private static WebSocketServer wssv;
        static void Main(string[] args)
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            currentDomain.UnhandledException += CurrentDomain_UnhandledException;
            (new Program()).Run();
            
            
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            
            
            Console.WriteLine("\nError: ");
        }

        private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //This handler is called only when the common language runtime tries to bind to the assembly and fails.
            var service = ModuleDataExchanger.WebSocketServices.FirstOrDefault(serviceItm => serviceItm.Name.ToLower() == args.Name.ToLower());

            if (service == null)
            {
                Assembly executingAssembly = Assembly.GetExecutingAssembly();

                string applicationDirectory = Path.GetDirectoryName(executingAssembly.Location);

                string[] fields = args.Name.Split(',');
                string assemblyName = fields[0];
                string assemblyCulture;
                if (fields.Length < 2)
                    assemblyCulture = null;
                else
                    assemblyCulture = fields[2].Substring(fields[2].IndexOf('=') + 1);


                string assemblyFileName = assemblyName + ".dll";
                string assemblyPath;

                if (assemblyName.EndsWith(".resources"))
                {
                    // Specific resources are located in app subdirectories
                    string resourceDirectory = Path.Combine(applicationDirectory, assemblyCulture);

                    assemblyPath = Path.Combine(resourceDirectory, assemblyFileName);
                }
                else
                {
                    assemblyPath = Path.Combine(applicationDirectory, assemblyFileName);
                }



                if (File.Exists(assemblyPath))
                {
                    //Load the assembly from the specified path.                    
                    Assembly loadingAssembly = Assembly.LoadFrom(assemblyPath);

                    //Return the loaded assembly.
                    return loadingAssembly;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                Assembly loadingAssembly = Assembly.LoadFrom(service.AssemblyPath);

                //Return the loaded assembly.
                return loadingAssembly;
            }
            
            
        }

        private static void ModuleDataExchanger__serviceClosed(WebsocketServiceAgent serviceAgent)
        {
            serviceAgent = null;
        }

        
        private static clsOntologyItem Initialize()
        {
            serviceAgentDb = new ServiceAgent_Elastic(localConfig);
            var result = serviceAgentDb.GetData_Port();
            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            serviceItems = serviceAgentDb.GetServices();

            if (serviceItems == null || !serviceItems.Any()) return localConfig.Globals.LState_Error.Clone();

            result = serviceAgentDb.SetUserGroupPath();

            if (result.GUID == localConfig.Globals.LState_Error.GUID) return result;

            result = serviceAgentDb.GetData_ComServerIp();

            return result;

        }

        void Run()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            var result = Initialize();

            if (result.GUID == localConfig.Globals.LState_Relation.GUID)
            {
                Console.WriteLine("\nThe Port is not configured!");
                Environment.Exit(-1);
            }
            else if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                Console.WriteLine("\nAn Error occured!");
                Environment.Exit(-1);
            }
            else
            {
                WebSocketBase.ComServerIP = serviceAgentDb.ComServerIp;
                WebSocketBase.ComServerPort = serviceAgentDb.Port;
                ModuleDataExchanger._serviceClosed += ModuleDataExchanger__serviceClosed;
                wssv = new WebSocketServer(serviceAgentDb.Port, true);
                wssv.SslConfiguration.ServerCertificate = new System.Security.Cryptography.X509Certificates.X509Certificate2("omodules.de.pfx", "S?r8b5$P6Rq!Em{");

#if DEBUG
                // To change the logging level.
                wssv.Log.Level = LogLevel.Trace;

                // To change the wait time for the response to the WebSocket Ping or Close.
                wssv.WaitTime = TimeSpan.FromSeconds(2);
#endif
                wssv.AddWebSocketService<ComServiceAgent>("/ModuleCommunicator");

                ModuleDataExchanger.WebSocketServices = serviceItems;
                
                ModuleDataExchanger.WebSocketServices.ForEach(serviceItem =>
                {
                    if (!serviceItem.IsActive) return;

                    serviceItem.WebSocketControllers.ForEach(controller =>
                    {
                        serviceItem.WebSocketPath = "/" + serviceItem.Name + "." + controller.Name;
                        wssv.AddWebSocketService<WebsocketServiceAgent>(serviceItem.WebSocketPath);

                    });

                });

               
                wssv.Start();
                if (wssv.IsListening)
                {
                    Console.WriteLine("Listening on port {0}, and providing WebSocket services:", wssv.Port);
                    foreach (var path in wssv.WebSocketServices.Paths)
                        Console.WriteLine("- {0}", path);
                }
                Console.WriteLine("\nPress Enter key to stop the server...");
                Console.ReadLine();
               


            }
        }
    }
}
